#!/bin/bash

source_directory="$(cd $(dirname ${BASH_SOURCE[0]}) && pwd)"
eis_directory="${HOME}/src/EditionInSitu"
execution_options=""
option_c="${source_directory}/eis_config.json"

for i in "$@"; do
    case $i in
	-c)
	    echo -e "Hello\n\tOption $i is already being used by default ($option_c).\n\tBut if you really want this, you may have to modify this script to suit\nyour needs"
            exit 1
            ;;
	*)
	    execution_options="${execution_options} $i"
	    ;;
    esac
done

cd ${eis_directory}/bin
./eisclient ${execution_options} -c "$option_c"
