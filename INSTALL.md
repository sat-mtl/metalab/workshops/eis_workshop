Full fledged installation guide
===============================

Below are the instructions for the full setup of the immersive space. The installation will guide you through the build process on Ubuntu 18.04.

# Splash computer

First, install [Lubuntu](https://lubuntu.net/downloads/)
Boot the computer, and install nvidia drivers:
```bash
sudo apt install nvidia-dkms-418 nvidia-driver-418 nvidia-kernel-common-418 nvidia-kernel-source-418
reboot
```
Install `git`:
```bash
sudo apt install git
```
## Splash installation:

```bash
mkdir src && cd src
git clone https://gitlab.com/sat-mtl/infra/splash-configs.git
cd splash-configs/tools
./build_splash master
```
To test Splash:
```bash
splash
```

You will need Blender (version 2.79) to prepare the Splash configuration:

```bash
sudo apt install blender
```
Then install the addon (from the Preferences window). The addon is located in `${HOME}/src/splash/build/addons/blender/blenderSplash.zip`


# EiS computer

First, install [Lubuntu](https://lubuntu.net/downloads/)
Boot the computer, and install nvidia drivers:
```bash
sudo apt install nvidia-dkms-418 nvidia-driver-418 nvidia-kernel-common-418 nvidia-kernel-source-418
reboot
```

Install `git`:
```bash
sudo apt install git
```

## EiS installation:

Run the quick installation script
```bash
mkdir src && cd src
git clone https://gitlab.com/sat-metalab/EditionInSitu
cd EditionInSitu
./setup.sh
```

## SATIE installation:

### Install dependencies

Use this command to install the packages:
```bash
sudo apt-get install build-essential libsndfile1-dev libasound2-dev libjack-jackd2-dev \
libavahi-client-dev libicu-dev libreadline6-dev libfftw3-dev libxt-dev libudev-dev pkg-config cmake \
qt5-default qt5-qmake qttools5-dev qttools5-dev-tools qtdeclarative5-dev qtpositioning5-dev \
libqt5sensors5-dev libqt5opengl5-dev qtwebengine5-dev libqt5svg5-dev libqt5websockets5-dev
```
### Clone SuperCollider

You will need to clone the SuperCollider Github repository and fetch the project's submodules.
Run the following commands:
```
cd ~/src
git clone --branch master --recurse-submodules https://github.com/supercollider/supercollider.git
```

### Build and install

Create a build folder, then configure, make, and install:
```
cd ~/src/supercollider
mkdir build && cd build
cmake -DCMAKE_BUILD_TYPE=Release -DNATIVE=ON -DSC_EL=OFF ..   # turn off emacs-based IDE
make -j$(nproc) && sudo make install
sudo ldconfig    # needed when building SuperCollider for the first time
```

### Installing sc3-plugins

Like SuperCollider, sc3-plugins 3.10 isn't currently packaged for Linux.
You will need to clone the sc3-plugins Github repository and fetch the project's submodules.
```
cd ~/src
git clone --branch master --recurse-submodules https://github.com/supercollider/sc3-plugins.git
cd sc3-plugins
```

Create a build folder, then configure, build, and install:

```
mkdir build && cd build
cmake -DSC_PATH=../../supercollider/ -DSUPERNOVA=ON -DCMAKE_BUILD_TYPE=Release -DNATIVE=ON ..
make -j$(nproc) && sudo make install
```
### Install SATIE

The SATIE quark can be installed directly from within the SuperCollider IDE (scide).

In a new document, write the following line of code and evaluate it by hitting `Ctrl-Enter`:

```
Quarks.install("SATIE");
```

## HTC Vive Controllers Setup (Optional)
To use the HTC Vive controllers, follow the instructions below. Note that EiS does not require the controllers. It can be used with a keyboard and a mouse.

### Install Steam and SteamVR
Install Steam from [STEAM](https://store.steampowered.com/about/).
And SteamVR from the [Steam Store](https://store.steampowered.com/app/250820/SteamVR/).

You need the latest nvidia drivers (at least 430.26) to be able to run SteamVR on Linux:
```
sudo add-apt-repository ppa:graphics-drivers/ppa
sudo apt update
sudo apt install nvidia-430  # or higher if available
```

### Install VRPN
```
cd ~/src
git clone https://gitlab.com/sat-metalab/vrpn
cd vrpn
mkdir build && cd build
cmake ..
make -j$(nproc) && sudo make install
```

### Install VRPN-OpenVR
```
cd ~/src
git clone https://gitlab.com/sat-metalab/vrpn-openvr.git
cd vrpn-openvr
git submodule update --init --recursive
mkdir build && cd build
cmake ..
make -j$(nproc)
```

### SteamVR without headset
To allow using SteamVR without a headset, for example to use only the VR controllers, you have to enable the NULL driver. Edit the file located in `${HOME/.local/share/Steam/config/steamvr.vrsettings` and replace its content with the following:
```json
{
   "jsonid" : "vrsettings",
   "steamvr" : {
      "directModeEdidPid" : 43521,
      "directModeEdidVid" : 53794,
      "forcedDriver" : "null",
      "requireHmd" : true,
      "enableHomeApp" : false,
      "activateMultipleDrivers" : true
   },
   "driver_null" : {
      "enable" : true,
      "id" : "Null Driver",
      "serialNumber" : "Null 4711",
      "modelNumber" : "Null Model Number",
      "windowX" : 100,
      "windowY" : 100,
      "windowWidth" : 1920,
      "windowHeight" : 1080,
      "renderWidth" : 1280,
      "renderHeight" : 720,
      "secondsFromVsyncToPhotons" : 0.1,
      "displayFrequency" : 60
   }
}
```
