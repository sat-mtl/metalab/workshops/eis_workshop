EiS workshop repository
======================
This repository holds everything there is to know/have to reproduce the workshop which was held during the 2019 iX symposium at the [Society for Arts and Technology](https://sat.qc.ca), and which was titled "Rapid prototyping of an immersive audiovisual installation". In this short document you will find information about the directory structure, the software used during the workshop and how they were used.


Software involved in the workshop
=================================
This workshop made use of a lot of the [Society for Arts and Technology](https://sat.qc.ca) Metalab's software. Besides the [In Situ Edition](https://gitlab.com/sat-metalab/EditionInSitu) (or EiS) software, were involved a projection mapping engine ([Splash](https://gitlab.com/sat-metalab/splash)), a sound spatialization engine ([SATIE](https://gitlab.com/sat-metalab/satie)), [Switcher](https://gitlab.com/sat-metalab/switcher) for reading audio and video files, and [vrpn-openvr](https://gitlab.com/sat-metalab/vrpn-openvr) for handling the HTC Vive controllers

Overall the architecture looks like this:

      ┌───────────────┐ HTC Vive controls
      │  vrpn-openvr  │──────────────────┐
      └───────────────┘                  │
                                         ↓
      ┌────────────┐  Media reading  ┌───────┐   Video output   ┌──────────┐
      │  Switcher  │ ──────────────→ │  EiS  │ ───────────────→ │  Splash  │
      └────────────┘                 └───────┘                  └──────────┘
                                         │
                                         │     OSC messages     ┌─────────┐
                                         └────────────────────→ │  SATIE  │
                                                                └─────────┘

The version of each software used in this workshop is the following:
* EiS: 0.1.0
* Splash: 0.7.20
* SATIE: commit eeb34800390cc01d3184d182ecdbb36ca25c0dca
* switcher: commit f92991576556c6545363e4cf1e3ce9c714cf2bcc
* shmdata: commit 2a681bdf9b3de5619864df708a9ccd1019ab29ed


Directory structure
===================
* assets: all the assets used during the workshop (and some assets which were not used)
* projects: the resulting projects saved in the [EiS](https://gitlab.com/sat-metalab/EditionInSitu) format, which can be loaded by EiS
* satie: [SATIE](https://gitlab.com/sat-metalab/satie) configuration to use with EiS
* splash: [Splash](https://gitlab.com/sat-metalab/splash) configuration for the immersive space


Cloning the project
===================
Assuming that you have installed Git on your computer, cloning this project on your computer is done as follows:
```bash
git clone https://gitlab.com/sat-metalab/workshops/eis_workshop
cd eis_workshop
git lfs fetch && git lfs checkout
```

Note that all the software mentionned in this documentation work on Ubuntu 18.04. Although a few of them can run on other operating systems, this workshop needs the latest LTS version of Ubuntu for every software to run as expected.


Installation
============
See instructions in [INSTALL](INSTALL.md)

Running the project
===================
To run the project (and after having installed all the software), you have to run the server and the client using the following scripts. The HTC Vive controllers are optinal. EiS can be used with the mouse and keyboard.

```bash
./run_server.sh
# In another terminal
./run_client.sh
```

If you are using the HTC Vive controllers, first launch Steam and SteamVR. You can now pair the controllers.
Execute the VRPN server:
```
cd ~/src/vrpn-openvr/build
~/.steam/ubuntu12_32/steam-runtime/run.sh ./vrpnOpenVR
```

You should see a stream of informations on the state of the headset and controllers, similar to this:
```
[openvr/hmd/0] Invalid Pose
[openvr/controller/LHR-FFF1B943] OK
[openvr/controller/left] OK
[openvr/controller/LHR-FD76D946] OK
[openvr/controller/right] OK
[openvr/hmd/0] Invalid Pose
[openvr/controller/LHR-FFF1B943] OK
[openvr/controller/left] OK
[openvr/controller/LHR-FD76D946] OK
[openvr/controller/right] OK
[openvr/hmd/0] Invalid Pose
[openvr/controller/LHR-FFF1B943] OK
[openvr/controller/left] OK
[openvr/controller/LHR-FD76D946] OK
[openvr/controller/right] OK
```
Note the controllers ID, you will need it for the next step.

To prevent left and right inversion of the controllers, edit the file located in `~/src/EditionInSitu/config/local.json` and specify the controllers IDs as follow:
```json
"editor": {
    "input.vive.enabled": true,
    "input.vive.offset.location": [0.0, 0.0, 0.0],
    "input.vive.offset.rotation": [0.0, 0.0, 0.0],
    "input.vive.offset.tracker_rotation": [0.0, 0.0, 0.0],
    "input.vive.vrpn.host": "rhea.local",
    "input.vive.vrpn.primary.device": "openvr/controller/LHR-FD77D946",
    "input.vive.vrpn.secondary.device": "openvr/controller/LHR-FFF1B943"
}
```

To load the projects created during the workshop, you first have to copy the saved files into a specific directory known to EiS:
```bash
mkdir -p ${HOME}/.eis/scenes
cp projects/* ${HOME}/.eis/scenes/
```

Then go into the 'Save/Load' menu in EiS, then 'Load scene', and you should find and be able to load them.


Licence
=======
All the code in this repository is under the GNU Public Licence: [GPL](https://www.gnu.org/licenses/gpl-3.0.en.html).

All the content is under the [CC-BY](https://creativecommons.org/licenses/by/2.0/) licence.


Credits
=======

Assets taken from [Sketchfab](https://sketchfab.com):
- Empty pine cone model: 3dhdscan,  licence: CC Attribution-NonCommercial-NoDerivs
- Radial Symmetry Test: Renafox , licence: CC Attribution
- Cob: luyssport, licence: CC Attribution-NonCommercial-ShareAlike
- Baba Yaga's Hut (scene and assets): Inuciian, licence: CC Attribution

Assets created/contributed during the iX 2019 workshop:
- Various cats images and videos: Megan Jones, Ludovic Harnois Lebeau
- Sounds: Erick Montgomery, Aleksandar Zecevic, Timothy Schrock, Olivier Lalonde, Alexandre Klinke, Megan Jones
- 3D models for the minidome project: Jean-Philippe Lafontaine
