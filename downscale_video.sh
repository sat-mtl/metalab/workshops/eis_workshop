#!/bin/bash

source_directory="$(cd $(dirname ${BASH_SOURCE[0]}) && pwd)"
input_video=${1}
fullname=$(basename "${input_video}")
filename=${fullname%.*}

ffmpeg -i ${input_video} -vf scale=-2:480 -c:v h264 -preset slower -q 1 ${HOME}/src/ixworkshop/assets/database/videos/${filename}.mov
